package strings

import (
	"math/rand"
	"strings"
	"time"
	"unsafe"
)

// random string generation. Adopted from:
// https://stackoverflow.com/questions/22892120/how-to-generate-a-random-string-of-a-fixed-length-in-go
const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
const (
	letterIdxBits = 6                    // 6 bits to represent a letter index
	letterIdxMask = 1<<letterIdxBits - 1 // All 1-bits, as many as letterIdxBits
	letterIdxMax  = 63 / letterIdxBits   // # of letter indices fitting in 63 bits
)

var src = rand.NewSource(time.Now().UnixNano())

// RandomString generates a random string og length n consisting of characters
// a-z, A-Z and 0-9.
func RandomString(n int) string {
	b := make([]byte, n)
	for i, cache, remain := n-1, src.Int63(), letterIdxMax; i >= 0; {
		if remain == 0 {
			cache, remain = src.Int63(), letterIdxMax
		}
		if idx := int(cache & letterIdxMask); idx < len(letterBytes) {
			b[i] = letterBytes[idx]
			i--
		}
		cache >>= letterIdxBits
		remain--
	}

	return *(*string)(unsafe.Pointer(&b))
}

// SplitMulti slices s into all substrings separated by any character of sep
// and returns a slice of the substrings between those separators.
// If s does not contain any character of sep and sep is not empty, SplitMulti
// returns a slice of length 1 whose only element is s.
// If sep is empty, SplitMulti splits after each UTF-8 sequence. If both s and
// sep are empty, SplitMulti returns an empty slice.
func SplitMulti(s string, sep string) []string {
	var a []string

	// handle special cases: if sep is empty ...
	if len(sep) == 0 {
		//... and if s is empty: return an empty slice
		if len(s) == 0 {
			return a
		}
		// ... else split after each character
		return strings.Split(s, "")
	}

	// split s by the characters of sep
	for i, j := -1, 0; j <= len(s); j++ {
		if j == len(s) || strings.Contains(sep, string(s[j])) {
			if i+1 > j-1 {
				a = append(a, "")
			} else {
				a = append(a, s[i+1:j])
			}
			i = j
		}
	}

	// if s does not contain any character of sep: return a slice that only
	// contains s
	if len(a) == 0 {
		a = append(a, s)
	}

	return a
}
