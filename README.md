[![Go Reference](https://pkg.go.dev/badge/gitlab.com/go-utilities/file.svg)](https://pkg.go.dev/gitlab.com/go-utilities/strings)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/go-utilities/file)](https://goreportcard.com/report/gitlab.com/go-utilities/strings)
[![REUSE status](https://api.reuse.software/badge/gitlab.com/go-utilities/file)](https://api.reuse.software/info/gitlab.com/go-utilities/strings)

# Go Utilities: strings

Go package with utilities that support dealing with strings, extending the functionality of the [standard package](https://pkg.go.dev/strings).
